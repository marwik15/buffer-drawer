﻿#pragma once
#include <math.h>
#include <iostream>
#include <string>

#define STB_TRUETYPE_IMPLEMENTATION
#include "stb_truetype.h"

#define STB_IMAGE_WRITE_IMPLEMENTATION
#include "stb_image_write.h"

struct PixelColor {
	unsigned char r, g, b, w;
	PixelColor() {
        r = g = b = 255;
	    w = 255; }

	PixelColor(int r, int g, int b, int w) : r(r), g(g),b(b),w(w) {}
    PixelColor(int r, int g, int b) : r(r), g(g), b(b), w(255) {}
};

class BufferDrawer {
private:
    int width, height;

    PixelColor* dbBuffer;

    void sizeGuard(int& x,int& y) {
        if (x > width-1) x = width-1;
        if (y > height-1) y = height-1;
    }

public:
    BufferDrawer() {}

    BufferDrawer(int width,int height) {
        dbCreateBuffer(width, height);
    }

    BufferDrawer(int width, int height, PixelColor backgroundColor) {
        dbCreateBuffer(width, height);
        dbClearBuffer(backgroundColor);
    }

    ~BufferDrawer() {
        dbDeleteBuffer();
    }

    void dbCreateBuffer(int w,int h) {
	    width = w; height = h;
	    dbBuffer = new PixelColor[width * height*4];
    }

    void dbPutPixelColor(int x, int y, PixelColor p = PixelColor(255, 255, 255, 255)) {
        sizeGuard(x,y);
	    dbBuffer[y * width + x] = p;
    }

    void dbClearBuffer(PixelColor p = PixelColor()) {
	    for (int y = 0; y < height; y++) {
		    for (int x = 0; x < width; x++) {
			    dbPutPixelColor(x,y, p);
		    }
	    }
    }

    PixelColor* dbGetBuffer() {
	    return dbBuffer;
    }

    void dbSaveBufferPNG(std::string path = "buffer.png") {
        stbi_write_png(path.c_str(), width, height, 4, dbBuffer, width * 4);
    }

    void dbSaveBufferBMP(std::string path = "buffer.bmp") {
        stbi_write_bmp(path.c_str(), width, height, 4, dbBuffer);
    }

    void dbSaveBufferJPG(std::string path = "buffer.bmp",int quality = 100) {
        stbi_write_jpg(path.c_str(), width, height, 4, dbBuffer, quality);
    }

    void dbFillArea(int x, int y, int width, int height, PixelColor p = PixelColor()) {

        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                dbPutPixelColor(x +j, y+i,p);
            }
        }
    }

    void dbDrawline(const int x1, const int y1, const int x2, const int y2, PixelColor p = PixelColor()) {

        float  x, y,dx, dy, step;
        int i;

        dx = abs(x2 - x1);
        dy = abs(y2 - y1);

        if (dx >= dy)
            step = dx;
        else
            step = dy;

        dx = dx / step;
        dy = dy / step;

        x = x1;
        y = y1;

        i = 1;
        while (i <= step){
            dbPutPixelColor(x, y, p);

            x = x + dx;
            y = y + dy;
            i = i + 1;
        }
    }

    void dbDrawRectangle(int x,int y,int width,int height, PixelColor p = PixelColor()) {
	
	    dbDrawline(x,y,x+ width,y,p); // top 
	    dbDrawline(x,y+ height,x+ width,y+ height,p); // bottom

        dbDrawline(x, y, x, y + height,p); // left
        dbDrawline(x + width, y, x + width, y + height,p); // right
    }

    void dbDrawText(int x,int y) {
        long size;
        unsigned char* fontBuffer;

        //FILE* fontFile = fopen("cmunrm.ttf", "rb");
        FILE* fontFile = fopen("ReemKufi-Regular.ttf", "rb");
        fseek(fontFile, 0, SEEK_END);
        size = ftell(fontFile); /* how long is the file ? */
        fseek(fontFile, 0, SEEK_SET); /* reset */

        fontBuffer = (unsigned char*)malloc(size);

        fread(fontBuffer, size, 1, fontFile);
        fclose(fontFile);

        /* prepare font */
        stbtt_fontinfo info;
        if (!stbtt_InitFont(&info, fontBuffer, 0)){
            printf("failed\n");
        }

        int b_w = 512; /* bitmap width */
        int b_h = 128/2; /* bitmap height */
        int l_h = 64; /* line height */

        /* create a bitmap for the phrase */
        unsigned char* bitmap = (unsigned char*)calloc(b_w * b_h, sizeof(unsigned char));
        //unsigned char* bitmap = (unsigned char* )malloc(b_w*b_h*sizeof(unsigned char));


        /* calculate font scaling */
        float scale = stbtt_ScaleForPixelHeight(&info, l_h);

        std::wstring word = L"the quick brown fłox";

        int X = 0;

        int ascent, descent, lineGap;
        stbtt_GetFontVMetrics(&info, &ascent, &descent, &lineGap);

        ascent = roundf(ascent * scale);
        descent = roundf(descent * scale);

        for (int i = 0; i < word.size(); ++i) {
            /* how wide is this character */
            int ax;
            int lsb;
            stbtt_GetCodepointHMetrics(&info, word[i], &ax, &lsb);
            /* (Note that each Codepoint call has an alternative Glyph version which caches the work required to lookup the character word[i].) */

            /* get bounding box for character (may be offset to account for chars that dip above or below the line */
            int c_x1, c_y1, c_x2, c_y2;
            stbtt_GetCodepointBitmapBox(&info, word[i], scale, scale, &c_x1, &c_y1, &c_x2, &c_y2);

            /* compute y (different characters have different heights */
            int y = ascent + c_y1;

            /* render character (stride and offset is important here) */
            int byteOffset = X + roundf(lsb * scale) + (y * b_w);
            stbtt_MakeCodepointBitmap(&info, bitmap + byteOffset, c_x2 - c_x1, c_y2 - c_y1, b_w, scale, scale, word[i]);


            /* advance X */
            X += roundf(ax * scale);

            /* add kerning */
            int kern;
            kern = stbtt_GetCodepointKernAdvance(&info, word[i], word[i + 1]);
            X += roundf(kern * scale);
        }

        for (int j = 0; j < b_h; j++) {
            for (int i = 0; i < b_w; i++) {
                auto currentBWpyxel = bitmap[j * b_w + i];
                
                if (currentBWpyxel != 0) {
                    dbPutPixelColor(i + x, j + y, PixelColor(255, 255, 255, currentBWpyxel));
                }
            }
        }

        /*
         Note that this example writes each character directly into the target image buffer.
         The "right thing" to do for fonts that have overlapping characters is
         MakeCodepointBitmap to a temporary buffer and then alpha blend that onto the target image.
         See the stb_truetype.h header for more info.
        */

        delete fontBuffer;
        delete bitmap;
    }

    void dbDeleteBuffer() {
        delete[] dbBuffer;
    }

};