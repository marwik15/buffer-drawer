#include "bufferDrawer.hpp"

using namespace std;

int main() {
	BufferDrawer bf;

	bf.dbCreateBuffer(600,600);
	
	bf.dbClearBuffer(PixelColor(207, 202, 201, 255));

	bf.dbPutPixelColor(0, 0,PixelColor(255,100,50,255));
	bf.dbPutPixelColor(1, 1,PixelColor(255,100,50,255));
	bf.dbPutPixelColor(2, 2,PixelColor(255,100,50,255));

	bf.dbDrawline(10, 10, 100, 50);
	bf.dbDrawline(0, 0, 100, 100);
	bf.dbDrawline(123, 250, 350, 260);
	
	bf.dbDrawRectangle(10, 20, 100, 100);
	
	bf.dbFillArea(200, 30, 150, 300,PixelColor(0,0,0,255));
    bf.dbDrawRectangle(200, 30, 150, 300);
    
	bf.dbDrawText(20,200);

	bf.dbSaveBufferPNG();

	bf.dbDeleteBuffer();
}